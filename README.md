# Conway GoL

Ce projet est **un exercice de prise en main de Rust**. Il me sert à pouvoir démontrer mes compétences rustacéennes, me donner confiance en moi dans cette technologie et me permettre de m'essayer à sa pratique.

Pourquoi le jeu de la vie de Conway ? Parce qu'il demande de construire un minimum de **structures réfléchies** et propose une **interface visuelle**, facile à présenter. Avec ces contraintes le jeu de la vie présente un minimum de travail graphique pour avoir un résultat parlant. De plus avec son parcours de cellules à chaque tour et leur interconnexion des problèmes d'optimisaion se posent. Je n'ai pas fait le code le plus optimal qui soit faute de temps et d'énergie mais je vais continuer à l'améliorer et je reste ouvert pour en discuter avec quiconque lit ces lignes.

Dans le cadre de cet exercice j'ai :
- tenté de respecter au mieux les normes de la Communauté rustacéenne.
- commenté la partie publique de l'API du code.
- établi des tests unitaires sur la partie algorithmique du projet.
- établi un benchmark afin d'améliorer le projet (avec criterion).
- exploité un git organisé autour de 2 branches, main et dev, avec des commits assez propres.
- utilisé une bibliothèque graphique (femtovg).
- utilisé les outils propres à Rust suivants :
    - la possession et l'emprunt
    - les crates
    - RefCell (pour stocker les cellules dans la map, et visiter les voisinnes sans faire de clone)
    - cargo
    - des macros

Grand amateur d'OCaml je retrouve en Rust une approche logique saine mais puissante et bien pensée !

## Requirements

Requiert:
- Rust : ^1.75

## Installation

``cargo build``

https://doc.rust-lang.org/cargo/guide/working-on-an-existing-project.html

## Utilisation

L'éxécution du binaire généré par build permet la mise en place d'un exemple.

### Commandes *ingame*

- ***SPACE*** permet de mettre le jeu en pause/annuler la pause. Le jeu commence en pause dans l'exemple.
- Le ***clic gauche*** vous permet de faire basculer l'état d'une case du jeu. A utiliser avec le jeu en pause pour plus d'intérêt.

### Librairie

Utiliser ``cargo doc --lib`` pour générer la dernière documentation automatiquement.
