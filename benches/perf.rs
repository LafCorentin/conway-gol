use criterion::{black_box, criterion_group, criterion_main, Criterion};
use conway_gol::GameBuilder;

// fn bench_fibs(c: &mut Criterion) {
//     let mut group = c.benchmark_group("Fibonacci");
//     for i in [20u64, 21u64].iter() {
//         group.bench_with_input(BenchmarkId::new("Recursive", i), i, 
//             |b, i| b.iter(|| fibonacci_slow(*i)));
//         group.bench_with_input(BenchmarkId::new("Iterative", i), i, 
//             |b, i| b.iter(|| fibonacci_fast(*i)));
//     }
//     group.finish();
// }


pub fn criterion_benchmark(c: &mut Criterion) {
    let width = 700;
    let height = 700;

    let mut test_game = GameBuilder::new(width, height, 1.);

    test_game.set_animation(false);
    test_game.set_pseudo_rng(false);
    
    let mut game1 = test_game.build();
    game1.try_rdm_alive(width*height/5);

    c.bench_function("perf_rdm_grid", |b| b.iter(|| black_box(game1.bench_run(1))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);