mod animation;
mod network;

use rand::SeedableRng;
use rand::{thread_rng, Rng};
use rand::rngs::StdRng;
use std::mem::replace;
use std::u64::MAX;

use animation::Animation;
use femtovg::{renderer::OpenGl, Canvas, Color, Paint, Path};
use glutin::surface::GlSurface;
use network::Network;
use winit::{
    event::{ElementState, Event, KeyboardInput, MouseButton, VirtualKeyCode, WindowEvent},
    event_loop::ControlFlow,
};
/// La fonction d'évaluation de l'état d'une cellule originale de Conway
pub fn eval_conway_classic(alive: bool, value: i8) -> bool {
    (alive && (value == 2 || value == 3)) || (!alive && value == 3)
}

pub const DEAD_COLOR: Color = Color::white();
pub const ALIVE_COLOR: Color = Color::black();

pub struct GameBuilder {
    width: u32,
    height: u32,
    size_square: f32,
    life_function: fn(bool, i8) -> bool,
    have_animation: bool,
    pseudo_rng: bool,
    title: &'static str,
}

/// Un objet permettant d'instancier un jeu après l'avoir paramétré
impl GameBuilder {
    pub fn new(width:u32, height: u32, size_square: f32) -> Self {
        GameBuilder {
            width,
            height,
            size_square,
            life_function: eval_conway_classic,
            have_animation: true,
            pseudo_rng: false,
            title: "Conway's Game of Life"
        }
    }

    pub fn build(&self) -> Game {
            let width = self.width;
            let height = self.height;
            let size_square = self.size_square;
            let life_function = self.life_function;

            let network = Network::new_toric(width, height);
            let animation = if self.have_animation {Some(Animation::new(
                self.width * self.size_square as u32,
                self.height * self.size_square as u32,
                self.title,
                false,
            ))} else {None};

            let rng = 
                StdRng::seed_from_u64(if self.pseudo_rng {
                    0
            } else {
                    thread_rng().gen_range(0..MAX)
            });
    
            Game {
                width,
                height,
                size_square,
                life_function,
                network,
                animation,
                rng
            }
    }

    pub fn set_title(&mut self, title: &'static str) {
        self.title = title;
    }

    pub fn set_animation(&mut self, have_animation: bool) {
        self.have_animation = have_animation;
    }

    pub fn set_pseudo_rng(&mut self, pseudo_rng: bool) {
        self.pseudo_rng = pseudo_rng;
    }
}

pub struct Game {
    width: u32,
    height: u32,
    size_square: f32,
    life_function: fn(bool, i8) -> bool,
    network: Network,
    animation: Option<Animation>,
    rng: StdRng,
}

impl Game {
    /// Launch the game in its display window.
    /// 
    ///  # Arguments
    /// 
    ///  `start_paused` - specify if the game is paused at the begginning.
    pub fn run(mut self, start_paused: bool) {
        let mut animation = replace(&mut self.animation, None).expect("Cannot run without animation. Maybe try bench-run()");

        let mut mousex = 0.0;
        let mut mousey = 0.0;

        let size_window = animation.window.inner_size();

        let mut pause = start_paused;

        animation.event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Poll;

            match event {
                Event::LoopDestroyed => *control_flow = ControlFlow::Exit,
                Event::WindowEvent { ref event, .. } => match event {
                    WindowEvent::Resized(physical_size) => {
                        animation.surface.resize(
                            &animation.context,
                            physical_size.width.try_into().unwrap(),
                            physical_size.height.try_into().unwrap(),
                        );
                    }
                    WindowEvent::CursorMoved {
                        device_id: _,
                        position,
                        ..
                    } => {
                        mousex = position.x as f32;
                        mousey = position.y as f32;
                    }
                    WindowEvent::MouseInput {
                        button: MouseButton::Left,
                        state: ElementState::Released,
                        ..
                    } => {
                        self.network.alternate_state_cell(
                            mousex as u32 / self.size_square as u32,
                            mousey as u32 / self.size_square as u32,
                        );
                        animation.window.request_redraw();
                    }
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::Space),
                                state: ElementState::Pressed,
                                ..
                            },
                        ..
                    } => pause = !pause,
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    _ => (),
                },
                Event::RedrawRequested(_) => {
                    animation.canvas.clear_rect(
                        0,
                        0,
                        size_window.width,
                        size_window.height,
                        DEAD_COLOR,
                    );

                    self.draw_cells(&mut animation.canvas);

                    animation.canvas.flush();

                    animation.surface.swap_buffers(&animation.context).unwrap();
                }
                Event::MainEventsCleared => (),
                _ => {
                    if !pause {
                        self.network.run(self.life_function);
                    }
                    animation.window.request_redraw();
                }
            }
        });
    }

    /// Just run the game without output to measure perfs.
    /// 
    /// # Arguments
    /// 
    /// * `laps` - Number of game incrementations.
    pub fn bench_run(&mut self, laps: u32) {
        for _ in 0..laps {
            self.network.run(self.life_function);
        }
    }

    fn draw_cells(&self, canvas: &mut Canvas<OpenGl>) {
        let alive_square = Paint::box_gradient(
            0.,
            0.,
            self.size_square,
            self.size_square,
            0.,
            0.,
            ALIVE_COLOR,
            ALIVE_COLOR,
        );

        self.network
            .iter()
            .map(|(coo, rcell)| (coo, rcell.borrow().is_alive()))
            .for_each(|(coo, alive)| {
                if alive {
                    let mut path = Path::new();
                    path.rect(
                        self.size_square * coo.0 as f32,
                        self.size_square * coo.1 as f32,
                        self.size_square,
                        self.size_square,
                    );
                    canvas.fill_path(&path, &alive_square);
                }
            })
    }

    /// Try **number** times to change a random cell from dead to alive.
    ///
    /// # Arguments
    /// 
    /// * `number` - Numer of tries to find a cell dead randomly picked.
    ///  
    pub fn try_rdm_alive(&mut self, number: u32) {
        for _ in 0..number {
            let x = self.rng.gen_range(0..self.width - 1);
            let y = self.rng.gen_range(0..self.height - 1);

            if !self.network.is_cell_alive(x, y) {
                self.network.alternate_state_cell(x, y);
            }
        }
    }
}