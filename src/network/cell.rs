use std::collections::HashSet;

const DEFAULT_ALIVE: bool = false;

#[derive(PartialEq, Clone, Copy, PartialOrd, Debug, Hash, Eq, Ord)]
pub struct Coo(pub u32, pub u32);

pub struct Cell {
    coo: Coo,
    alive: bool,
    pub neighbours_alive: i8,
    pub neighbours: HashSet<Coo>,
}

impl PartialEq for Cell {
    fn eq(&self, other: &Self) -> bool {
        self.coo().eq(&other.coo())
    }
}

impl Cell {
    pub fn new(coo: Coo) -> Cell {
        Cell {
            coo,
            alive: DEFAULT_ALIVE,
            neighbours_alive: 0,
            neighbours: HashSet::new(),
        }
    }

    pub fn coo(&self) -> Coo {
        self.coo
    }

    /// Applique la f à la vie de la cellule et renvoi si on état a changé son nouvel état.
    pub fn update(&mut self, f: fn(bool, i8) -> bool) -> Option<bool> {
        let old_alive = self.alive;
        self.alive = f(self.alive, self.neighbours_alive);
        if self.alive != old_alive {
            Some(self.alive)
        } else {
            None
        }
    }

    pub fn is_alive(&self) -> bool {
        self.alive
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn two_cells_system() {
        let mut cell1 = Cell::new(Coo(0, 0));
        let mut cell2 = Cell::new(Coo(1, 0));

        cell1.neighbours.insert(cell2.coo());
        cell2.neighbours.insert(cell1.coo());

        assert_ne!(cell1.coo(), cell2.coo());
        assert_eq!(cell1.neighbours.get(&cell2.coo()), Some(&cell2.coo()));

        assert_eq!(cell1.is_alive(), false);
        assert_eq!(cell1.update(|_, _| false), None);
        assert_eq!(cell1.is_alive(), false);
        assert_eq!(cell1.update(|_, _| true), Some(true));
        assert_eq!(cell1.is_alive(), true);
    }

    #[test]
    fn respect_work() {
        let mut cell = Cell::new(Coo(0, 0));
        cell.neighbours_alive = 2;

        assert_eq!(cell.update(|_, n| n != 2), None);
        assert_eq!(cell.is_alive(), false);
        assert_eq!(cell.update(|_, n| n == 2), Some(true));
        assert_eq!(cell.is_alive(), true);
    }
}
