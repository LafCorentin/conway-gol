use conway_gol::*;
fn main() {
    let width = 200;
    let height = 120;
    let pixels_square = 5.;

    let mut game_builder = GameBuilder::new(
        width,
        height,
        pixels_square,
    );

    game_builder.set_title("My conway Game of Life");

    let mut game = game_builder.build();

    game.try_rdm_alive(4000);

    game.run(true);
}
