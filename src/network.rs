mod cell;
use cell::Cell;
pub use cell::Coo;

use std::{
    cell::RefCell,
    collections::{hash_map::Iter, HashMap, HashSet},
};

pub struct Network {
    cortex: HashMap<Coo, RefCell<Cell>>,
    activity: HashSet<Coo>,
}

impl Network {
    pub fn new_toric(width: u32, height: u32) -> Network {
        let mut cortex = HashMap::new();
        cortex.reserve(usize::try_from(width * height).unwrap());

        for t in 0..width {
            for i in 0..height {
                let rcell = RefCell::new(Cell::new(Coo(t, i)));
                let coo = rcell.borrow().coo();
                cortex.insert(coo, rcell);
            }
        }

        for t in 0..width {
            for i in 0..height {
                let neighbours = Network::toric_neighbouring_of(t, i, width, height);
                let rcell = cortex.get(&Coo(t, i)).unwrap();
                let mut cell = rcell.borrow_mut();
                cell.neighbours = neighbours;
                //cortex.get_mut(&Coo(t, i)).unwrap().neighbours = neighbours;
            }
        }

        Network {
            cortex,
            activity: HashSet::new(),
        }
    }

    fn toric_neighbouring_of(x: u32, y: u32, width: u32, height: u32) -> HashSet<Coo> {
        let mut neighbours = HashSet::new();
        neighbours.insert(Coo((width + x - 1) % width, (height + y - 1) % height));
        neighbours.insert(Coo((width + x - 1) % width, y));
        neighbours.insert(Coo((width + x - 1) % width, (y + 1) % height));
        neighbours.insert(Coo(x, (height + y - 1) % height));
        neighbours.insert(Coo(x, (y + 1) % height));
        neighbours.insert(Coo((x + 1) % width, (height + y - 1) % height));
        neighbours.insert(Coo((x + 1) % width, y));
        neighbours.insert(Coo((x + 1) % width, (y + 1) % height));
        neighbours
    }

    pub fn alternate_state_cell(&mut self, x: u32, y: u32) -> bool {
        let target = Coo(x, y);

        let cell = self.cortex.get(&target).unwrap();
        let alive = cell.borrow_mut().update(|state, _| !state).unwrap();

        let cell = self.cortex.get(&target).unwrap();
        cell.borrow().neighbours.iter().for_each(|coo| {
            let rcell = self
                .cortex
                .get(coo)
                .expect(&format!("{:?} not in range", coo));
            rcell.borrow_mut().neighbours_alive += if alive { 1 } else { -1 };
            self.activity.insert(coo.clone());
        });

        self.activity.insert(target.clone());

        alive
    }

    pub fn run(&mut self, work: fn(bool, i8) -> bool) {
        let mut to_update_count = HashMap::new();
        let mut next_run = HashSet::new();

        self.work_each_cell(work, &mut to_update_count, &mut next_run);

        to_update_count.iter().for_each(|(coo, valeur)| {
            let cell = self.cortex.get(&coo).unwrap();
            cell.borrow_mut().neighbours_alive += valeur;
        });

        self.activity = next_run;
    }

    pub fn iter(&self) -> Iter<'_, Coo, RefCell<Cell>> {
        self.cortex.iter()
    }

    fn work_each_cell(
        &mut self,
        work: fn(bool, i8) -> bool,
        to_update_count: &mut HashMap<Coo, i8>,
        next_run: &mut HashSet<Coo>,
    ) {
        self.activity.iter().for_each(|coo| {
            let rcell = self.cortex.get(&coo).unwrap();
            let mut cell = rcell.borrow_mut();

            //si la cellule a changé d'état on met à jour ses voisins et on les ajoute au prochain run
            if let Some(alive) = cell.update(work) {
                let value_to_add = if alive { 1 } else { -1 };
                cell.neighbours
                    .iter()
                    .map(|n| n.clone())
                    .for_each(|coo_neighbour| {
                        if let Some(old_value) = to_update_count.get(&coo_neighbour) {
                            to_update_count.insert(coo_neighbour, old_value + value_to_add);
                        } else {
                            to_update_count.insert(coo_neighbour, value_to_add);
                        }

                        next_run.insert(coo_neighbour);
                    });
                next_run.insert(cell.coo().clone()); //TODO pas tout le temps nécessaire (ex gol à la mort)
            };
        });
    }

    pub fn is_cell_alive(&self, x: u32, y: u32) -> bool {
        let rcell = self.cortex.get(&Coo(x, y)).unwrap();
        rcell.borrow().is_alive().clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn build_toric_network() {
        let mut network = Network::new_toric(5, 5);
        network.alternate_state_cell(0, 4);
        network.alternate_state_cell(4, 0);
        network.alternate_state_cell(1, 0);
        network.alternate_state_cell(2, 2);

        let cell00 = network.cortex.get(&Coo(0, 0)).unwrap();

        assert!(network.is_cell_alive(0, 4));
        assert!(!cell00.borrow().is_alive());

        assert_eq!(cell00.borrow().neighbours_alive, 3);

        static mut CPT: i32 = 0;

        network.run(|alive, n_nbr| {
            unsafe { CPT += 1 };
            !alive && n_nbr == 3
        });

        unsafe {
            assert_eq!(CPT, 23); // seules (0,2) et (4,2) ne sont pas visitées
        }

        for (coo, rcell) in &network.cortex {
            let alive = rcell.borrow().is_alive();

            if *coo == Coo(0, 0) {
                assert!(alive);
            } else {
                assert!(!alive);
            }
        }

        network.run(|alive, n_nbr| !alive && n_nbr == 3);

        for (_, rcell) in &network.cortex {
            assert!(!rcell.borrow().is_alive());
        }
    }
}
